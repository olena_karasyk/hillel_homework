import random
from abc import ABC, abstractmethod


class Unit(ABC):
    _name = None
    _hp = 100
    _dmg = 5
    _defence = 1
    _experience = 0
    _dmg_chance = 0
    _def_chance = 0

   # @property
   # def _hp(self):
   #     return self._hp

   # @_hp.setter
    def hp(self, enemy_defence, my_dmg):
        if self._hp + enemy_defence - my_dmg < 0:
            self._hp = 0
            return self
        else:
            self._hp -= (my_dmg - enemy_defence)

    def chance_gen(self, c_type):
    # c_type - відповідає за те, шанс на атаку, чи шанс на захист
        if c_type == 'defence':
            step = 100 / (self._def_chance * 100)
            i = 1
            while True:
                if round(step) == i:
                    yield 0
                    step += step
                else:
                    yield 1
                i += 1
        elif c_type == 'damage':
            step1 = 100 / (self._dmg_chance * 100)
            j = 1
            while True:
                if round(step1) == j:
                    yield 1
                    step1 += step1
                else:
                    yield 0
                j += 1


    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def attack(self):
        pass

    @abstractmethod
    def _get_dmg(self):
        pass

    @abstractmethod
    def _get_defence(self):
        pass


class Mage(Unit):

    def __init__(self, name, dmg, defence, experience, spell_list, dmg_chance, def_chance):
        self._name = name
        self._dmg = dmg
        self._defence = defence
        self._experience = experience
        self._spell_list = iter(spell_list)
        if int(dmg_chance) not in range(0, 1):
            raise ValueError
        else:
            self._dmg_chance = dmg_chance
        if int(def_chance) not in range (0, 1):
            raise ValueError
        else:
            self._def_chance = def_chance

    def get_spell(self):
        try:
            return next(self._spell_list)
        except:
            return None

    def _get_dmg(self):
        res = self.chance_gen('damage')
        my_spell = self.get_spell()
        if my_spell is None:
            return self._dmg * (1 + next(res))
        elif my_spell._affect_obj == 'dmg':
            return (self._dmg + my_spell.spell_action(self._experience)) * (1 + next(res))
        return self._dmg * (1 + next(res))

    def _get_defence(self): #захист
        my_spell = self.get_spell()
        if my_spell is None:
            return self._defence
        elif my_spell._affect_obj == 'defence':
            return self._defence + my_spell.spell_action(self._experience)
        else:
            return self._defence

    def attack(self, enemy):
        if not isinstance(enemy, Unit):
            raise TypeError
        enemy_res = enemy.chance_gen('defence')
        a = self._get_dmg() * next(enemy_res)
        b = enemy._get_defence()
        return enemy.hp(b, a)




class Knight(Unit):

    def __init__(self, name, dmg, defence, experience, dmg_chance, def_chance):
        self._name = name
        self._dmg = dmg
        self._defence = defence
        self._experience = experience
        if int(dmg_chance) not in range(0, 1):
            raise ValueError
        else:
            self._dmg_chance = dmg_chance
        if int(def_chance) not in range(0, 1):
            raise ValueError
        else:
            self._def_chance = def_chance

    def _get_dmg(self):
            res = self.chance_gen('damage')
            self._dmg = (self._dmg * (1 + self._experience * 0.1)) * (1 + next(res))
            return self._dmg

    def _get_defence(self):
        self._defence = self._defence + (1 + self._experience * 0.01)
        return self._defence

    def attack(self, enemy):
        if not isinstance(enemy, Unit):
            raise TypeError
        enemy_res = enemy.chance_gen('defence')
        a = self._get_dmg() * (1 + next(enemy_res))
        b = enemy._get_defence() * 0.5
        return enemy.hp(b, a)


class Stuff(ABC):
    _stuff_name = None
    _unit_class = None
    _affect_obj = None
    _affect_type = None

    @abstractmethod
    def __init__(self):
        pass


class Spell(Stuff):
    """
   Mage can use spell weather to improve personal defence for some time or to improve personal dmg
    """
    def __init__(self, stuff_name, unit_class, affect_obj, affect_type):
        self._stuff_name = stuff_name
        self._unit_class = unit_class
        if affect_obj not in ('health', 'dmg', 'defence'):
            raise ValueError
            print('Incorrect affect type')
        else:
            self._affect_obj = affect_obj
        if affect_type not in ('add', 'mult'):
            raise ValueError
            print('Incorrect affect type')
        else:
            self._affect_type = affect_type

    def spell_action(self, experience):
        if self._affect_obj == 'defence':
            def_coef = (experience * 0.1) + 2
            return def_coef
        elif self._affect_obj == 'dmg':
            dam_coef = experience * 0.2 + 3
            return dam_coef


class Battle:
    modifications = {
        Mage: {
            '_hp': 4,
            '_defence': 1,
            '_dmg': 2},
        Knight: {
            '_hp': 4,
            '_defence': 1,
            '_dmg': 2}
    }

    def __init__(self, unit1, unit2):
        if not isinstance(unit1, Unit) or not isinstance(unit2, Unit):
            raise Exception

        # self.units = (unit1, unit2)
        self.unit1 = unit1
        self.unit2 = unit2

    def set_mods_into_battle(self, location=None, daypart=None):
        return self.modifications

    def deploy_mods_into_unit(self, unit):
        key = unit.__class__
        mods = self.modifications[key]

        for i in mods:
            attr = getattr(unit, i, None)

            if isinstance(attr, (int, float)):

                val = attr + mods[i]
                setattr(unit, i, val)

        return unit

    def decide_first(self):
        x = [self.unit1, self.unit2]
        random.shuffle(x)
        return x

    def gener_next(self):
        while True:
            yield self.unit1, self.unit2
            yield self.unit2, self.unit1

    def do_battle(self):
        self.unit1 = self.deploy_mods_into_unit(self.unit1)
        self.unit2 = self.deploy_mods_into_unit(self.unit2)
        lst = self.decide_first()
        self.unit1 = lst[0]
        self.unit2 = lst[1]

        while True:
            if self.unit1.attack(self.unit2) is None:
                return self.unit2
            elif self.unit2.attack(self.unit1) is None:
                return self.unit1





first_spell = Spell('superspell', Mage, 'dmg', 'mult')


unit1 = Knight('Alan', 0, 8, 0, 0.81, 0.1)
unit2 = Knight('White', 3, 2, 0, 0.9, 0.9)

Big_battle = Battle(unit1, unit2)
battle_res = Big_battle.do_battle()
print('Winner:', battle_res._name)
